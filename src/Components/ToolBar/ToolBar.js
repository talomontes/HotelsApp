import { ToolbarAndroid } from 'react-native';
export default ItemList class ItemList extends Component {
  render() {
    return (
      <ToolbarAndroid
        logo={require('./app_logo.png')}
        title="AwesomeApp"
        actions={[{title: 'Settings', icon: require('./icon_settings.png'), show: 'always'}]}
        onActionSelected={this.onActionSelected} />
    )
  },
  onActionSelected(position) {
    if (position === 0) { // index of 'Settings'
      showSettings();
    }
  }
}