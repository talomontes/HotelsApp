import React, { Component} from 'react'
import { View,Image,FlatList} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome';
import ItemsHotel from '../ItemsHotel/ItemsHotel'

const myIcon = (<Icon name="rocket" size={30} color="#900" />)
export default class HotelList extends Component {
    constructor(props){
        super(props);
        this.state={
          list:this.props.HotelData
        }
      }
    render (){
        return (
        <View >
            <myIcon></myIcon>
            <FlatList
                    data={this.state.list}
                    renderItem={({ item, index }) => 
                    <ItemsHotel obHotelInfo = {item}></ItemsHotel>
                    }
                    keyExtractor={(item, index) => index}
                />
        </View>);
        }
}

export { HotelList };