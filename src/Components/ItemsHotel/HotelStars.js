import React, { Component} from 'react'
import { View,Image} from 'react-native'


const HotelStars = ({ hotelStars }) => {
	var stars = [];
	console.log("Estrellitas",hotelStars)
    for (var i = 0; i < hotelStars; i++) {
        stars.push(
            // <Icon
            //     key={i} name='star'
            //     size={12}
            //     style={{ paddingRight: 4 }}
			// />);
			<Image style = { { width: 50, height: 50 } } 
			source =  {{uri:'https://image.freepik.com/iconos-gratis/favorito-de-estrellas_318-31577.jpg'}}/>
		);
    }
    return (
        <View style={styles.stars}>
            {stars}
        </View>
    );
};

const styles = {
    stars: {
        flexDirection: 'row',
        alignItems: 'flex-start'
	}
};

export { HotelStars };