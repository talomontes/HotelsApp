import React, { Component} from 'react'
import { View,Text,Image} from 'react-native'
import HotelPrice from './HotelPrice'
import  {HotelStars}  from './HotelStars'


export default class ItemsHotel extends Component {
	constructor(props) {
	  super(props);
	  // this.props.text = 'argentino';
	  this.state = {};
	}
	componentWillMount(){
		this.setState({image:'https://media-cdn.tripadvisor.com/media/photo-s/08/cd/99/1a/hard-rock-hotel-ibiza.jpg',
	name: 'Hotel', price:'35.000',hotelStars: 3 });
	}
    render() {
        return (
        	<View>
        		<Text>
        			{this.props.obHotelInfo.name}
        		</Text>
						<Image 
						style = { { width: 50, height: 50 } } 
                        source =  {{uri:this.props.obHotelInfo.image}}/>
						<HotelPrice price={this.props.obHotelInfo.price}></HotelPrice>
						
						<HotelStars hotelStars={this.props.obHotelInfo.hotelStars}/>
            </View>
						
        );
    }	
}