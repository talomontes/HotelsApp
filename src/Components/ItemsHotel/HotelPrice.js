import React, { Component} from 'react'
import { View,Text,Image} from 'react-native'

export default class HotelPrice extends Component {
	constructor(props) {
	  super(props);
	  this.state = {};
	}
	componentWillMount(){
		this.setState({image:'https://st.depositphotos.com/1620766/1344/v/950/depositphotos_13445040-stock-illustration-vector-icon-moon.jpg'});
	}
    render() {
        return (
        	<View>
				<Text>COP </Text>
        		<Text>
        			{this.props.price}
        		</Text>
				<Image style = { { width: 50, height: 50 } } 
                    	source =  {{uri:this.state.image}}/>
            </View>
        );
    }	
}