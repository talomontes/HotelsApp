import fonts from './TextStyles.js';
import colors from './Colors.js';
import objects from './Objects.js';

module.exports = {fonts,colors,objects};