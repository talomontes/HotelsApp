import { StyleSheet } from 'react-native';
import colors from './Colors.js'

var objects = StyleSheet.create({
  stars: {
    color:{Styles.colors.yellow},
    paddingRight: 4
  },
  starsView: {
  	flexDirection: 'row',
    alignItems: 'flex-start'
  }

});

module.exports = {objects,starsView};
