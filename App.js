/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  FlatList
} from 'react-native';

// import ItemsHotel from './src/Components/ItemsHotel/ItemsHotel';
// import {HotelList} from './src/Components/HotelsList/HoltelsList';
import {HotelList} from './src/Components/HotelsList/HoltelsList';



export default class App extends Component{
  constructor(props){
    super(props);
    this.state={
      list:[
        {image:'https://media-cdn.tripadvisor.com/media/photo-s/08/cd/99/1a/hard-rock-hotel-ibiza.jpg',name: 'Hotel', price:'35.000',hotelStars: 3 },
      {image:'https://media-cdn.tripadvisor.com/media/photo-s/08/cd/99/1a/hard-rock-hotel-ibiza.jpg',name: 'Hotel', price:'36.000',hotelStars: 5 }]
    }
  }
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>
          Welcome to React Native!
        </Text>
       <HotelList HotelData={this.state.list}></HotelList> 
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
